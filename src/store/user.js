// 1. 引入创建仓库的函数
import { loginTestAPI } from '@/api/profile'
import { defineStore } from 'pinia'
import { ref } from 'vue'

// 2. 定义可以使用仓库的函数, 并暴露
export const useUserStore = defineStore(
    // 仓库标识
    'user',

    // 回调函数
    () => {
        // 储存用户数据的变量
        const profile = ref({})

        // 登录函数
        const loginTest = async () => {
            const { result } = await loginTestAPI('13866688877')
            profile.value = result
            // 储存完数据, 后退页面
            // 弹出提示, 稍等一会, 然后回退
            uni.showToast({
              title: '登录成功',
              icon: 'success'
            })
            setTimeout(() => {
              uni.navigateBack()
            }, 1000);
        }

        const setProfile = (data) => {
          profile.value = data
        }

        // 记得全部返回
        return {
            profile,
            loginTest,
            setProfile
        }
    },

    // 其余配置
    {
        persist: {
            enabled: true
        }
    }
)