import {defineStore} from 'pinia'
import {ref} from 'vue'

export const useSysInfoStore = defineStore('sysInfo', () => {
    
    // 获取安全区域
    const res = uni.getWindowInfo()
    const safeArea = ref(res.safeArea)
    // 获取胶囊区域
    const capButton = ref(uni.getMenuButtonBoundingClientRect())
    
    return {
        safeArea,
        capButton
    }
})