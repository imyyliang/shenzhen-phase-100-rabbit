import {useUserStore} from '@/store/user'
const store = useUserStore()

export const baseURL = 'https://pcapi-xiaotuxian-front-devtest.itheima.net'

export const http = async (options) => {
    // options 是外部传入的请求配置对象
    // {
    //     method: 'post',
    //     url: '/abc',
    //     data: {
    //         name: 'tom'
    //     }
    // }
    // 在发请求之前, 往url添加基地址
    // 如果你的请求地址不完整, 帮你补全, 如果你已经是完整的地址, 就直接使用
    // 请求之前的请求拦截器
    
    if (options.url.indexOf('http') !== 0) {
        options.url = baseURL + options.url
    }
    // 如果有token这里可以添加token或者其他的请求头
    options.header = {
        ...options.header,
        // 如果本来是有其他header,要恢复
        // 添加token
        // Authorization: 'Bearer lalalalal'
        // 这个项目专门跟后端原定, 标明客户端来源的属性
        "source-client": "miniapp"
    }

    // 处理token请求头
    if (store.profile.token) {
      options.header.Authorization = `Bearer ${store.profile.token}`
    }

    // 传入什么配置, 都原封不动交给 uni.request
    // 请求本体
    const res = await uni.request(options)

    // 根据返回的res进行判断处理, 实现响应拦截功能
    if (res.statusCode >= 200 && res.statusCode < 300) {
        // 如果成功,则数据剥离
        return res.data;
    }

    if (res.statusCode === 401) {
        // 如果报401就是登录失效(或者没登录)
        uni.navigateTo({ url: "/pages/login/index" });
        return
    }
}