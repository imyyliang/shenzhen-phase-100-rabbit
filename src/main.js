import { createSSRApp } from 'vue';
import * as Pinia from 'pinia';
import piniaPersist from 'pinia-plugin-persist-uni'
// 让全局样式重新生效
import App from "./App.vue";

export function createApp() {
	const app = createSSRApp(App);
	
	const pinia = Pinia.createPinia()
	pinia.use(piniaPersist)

	app.use(pinia);
	return {
		app,
		Pinia, // 此处必须将 Pinia 返回
	};
}