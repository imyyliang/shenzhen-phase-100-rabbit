import {http} from '@/utils/http'

export const getOrderPreAPI = () => {
  return http({
    url: '/member/order/pre'
  })
}

export const createOrderAPI = (data) => {
  return http({
    url: '/member/order',
    method: 'POST',
    data
  })
}

// 获取订单详情
export const geOrderDetailAPI = (id) => {
  return http({
    url: `/member/order/${id}`,
  });
};

// 获取支付参数
export const getPayParamsAPI = (orderId) => {
  // 取支付参数, 跟公司后端取
  return http({
    url: `/pay/wxPay/miniPay`, // 支付接口
    method: 'get',
    data: {
      orderId
    }
  })
}