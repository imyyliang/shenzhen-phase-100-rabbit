import {http} from '@/utils/http'

// 新增接口
export const addAddressAPI = (data) => {
  return http({
    method: 'post',
    url: '/member/address',
    data
  })
}

// 获取列表
export const getAddressListAPI = () => {
  return http({
    url: '/member/address',
  })
}

// 获取地址详情数据
export const getAddressDetailAPI = (id) => {
  return http({
    url: `/member/address/${id}`,
  })
}

// 更新地址数据
export const updateAddressAPI = (data) => {
  return http({
    method: 'put',
    url: `/member/address/${data.id}`,
    data
  })
}

// 删除地址
export const delAddressAPI = (id) => {
  return http({
    method: 'delete',
    url: `/member/address/${id}`
  })
}
