import {http} from '@/utils/http'

// 获取商品详情函数
export const getGoodsDetailAPI = (id) => {
  return http({
    url: '/goods',
    data: {
      id
    }
  })
}