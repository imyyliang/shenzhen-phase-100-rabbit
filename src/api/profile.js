import {http, baseURL} from '@/utils/http'
import { useUserStore } from '@/store/user'

// 正式登录演示
export const loginReal = (data) => {
    return http({
        method: 'post',
        url: '/login/wxMin',
        data
    })
}

// 内侧登录测试
export const loginTestAPI = (phoneNumber) => {
    return http({
        url: '/login/wxMin/simple',
        method: 'post',
        data: {
            phoneNumber
        }
    })
}

// 获取个人信息
export const getProfileAPI = async() => {
  return http({
    url: '/member/profile'
  })
}

// 上传头像
export const uploadFileAPI = async(filePath) => {
  // 获取仓库的操作要放在具体的函数内, 不能代码一加载就触发
  const userStore = useUserStore()

  return uni.uploadFile({
    // 主要注意的是, 这里已经不是 http 所以之前的
    // 请求拦截器已经不存在(基地址, 请求头)
    url: baseURL + '/member/profile/avatar',
    header: {
      Authorization: `Bearer ${userStore.profile.token}`
    },
    filePath,
    name: 'file',
  })
}

// 修改除了头像外的其他信息
export const updateProfileAPI = async (data) => {
  return http({
    method: 'put',
    url: '/member/profile',
    data
  })
}