import {http} from '@/utils/http'

// 获取分类
export const getCategoriesAPI = () => {
    return http({
        url: '/category/top'
    })
}