import { http } from '@/utils/http'

// 获取轮播图
export const getBannersAPI = (distributionSite = 1) => {
    return http({
        url: '/home/banner',
        data: {
            distributionSite
        }
    })
}

// 获取分类
export const getCategoryAPI = () => {
    return http({
        url: "/home/category/mutli",
    });
};

// 获取热门推荐
export const getHotsAPI = () => {
    return http({
        url: "/home/hot/mutli",
    });
};

// 获取新鲜好物
export const getNewGoodsAPI = (limit = 4) => {
    return http({
        url: "/home/new",
        data: { limit },
    });
};

// 猜你喜欢
export const getLikesAPI = (page = 1, pageSize = 10) => {
    return http({
        url: "/home/goods/guessLike",
        data: { page, pageSize },
    });
};

export const getRecommendAPI = (url) => {
    return http({
        url
    })
}