import {http} from '@/utils/http'

export const addCartAPI = (skuId, count) => {
  return http({
    url: "/member/cart",
    method: "post",
    data: { skuId, count },
  });
};

// 获取购物车数据列表
export const getCartListAPI = () => {
  return http({
    url: '/member/cart'
  })
}

// 修改单品状态
export const updateCartAPI = (data) => {
  return http({
    url: `/member/cart/${data.id}`,
    method: 'put',
    data
  })
}

// 全选接口
export const toggleAllSelectAPI = (selected) => {
  return http({
    url: '/member/cart/selected',
    method: 'put',
    data: {
      selected
    }
  })
}

// 删除商品API
export const deleteCartAPI = (id) => {
  return http({
    url: `/member/cart`,
    method: 'delete',
    data: { ids: [id] }
  }) 
}